interface Fan{
	command void start();//功能开启，需调用startDone
	
	event void startDone(error_t error);
	
	command void stop();//功能关闭，需调用stopDone
	
	event void stopDone(error_t error);
	
	command void set();

}
