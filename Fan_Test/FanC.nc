configuration FanC{
	provides interface Fan;
}
implementation{
	components FanP;
	Fan = FanP;

	components LedsC;
	FanP.Leds -> LedsC;

	components HplMsp430GeneralIOC as IoC;
	FanP.Port -> IoC.Port60;

}
