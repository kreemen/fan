#include"printf.h"
module FanTestC{
	uses{
		interface Fan;
		interface Boot;
		interface Leds;
        interface Timer<TMilli> as Timer0;
        interface Timer<TMilli> as Timer1;
	}
}
implementation{
	uint8_t counter;
	event void Boot.booted(){
        call Timer0.startPeriodic(10000);
        call Timer1.startPeriodic(80000);
		//call Fan.start();	
	}

   event void Timer0.fired() {
       call Fan.start();	
   }

     event void Timer1.fired() {
       call Fan.stop();	
   }
  
   event void Fan.startDone(error_t error){
		call Fan.set();
        printf("Fan is started !\n");
        printfflush();
	}
	
	event void Fan.stopDone(error_t error){
	    printf("Fan is stoped !\n");
        printfflush();
	}
	
}