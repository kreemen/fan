configuration FanTestAppC{
}
implementation{
	components MainC,FanTestC;
	FanTestC -> MainC.Boot;

	components LedsC;
	FanTestC.Leds -> LedsC;

	components FanC;
	FanTestC.Fan -> FanC;
  
    components new TimerMilliC() as Timer0;
    FanTestC.Timer0 -> Timer0;
  
    components new TimerMilliC() as Timer1;
    FanTestC.Timer1 -> Timer1;
  
}
