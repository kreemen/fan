module FanP{
	provides interface Fan;	
	uses{
		interface HplMsp430GeneralIO as Port;//60
		interface Leds;
	}
}
implementation{
	command void Fan.start(){
		call Port.makeOutput();
		call Port.clr();
		signal Fan.startDone(SUCCESS);	
	}

	command void Fan.stop(){
		call Port.clr();
		signal Fan.stopDone(SUCCESS);
	}

	default event void Fan.startDone(error_t error){
	
	}
	
	default event void Fan.stopDone(error_t error){
	
	}

	command void Fan.set(){
		call Port.set();
	}
	
}
