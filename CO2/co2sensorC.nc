configuration co2sensorC {  
  provides 
  {
    interface Read<uint16_t> as co2sensor; 
    interface SplitControl as co2Control;
	interface co2Info;
  }
}

implementation {
  components co2sensorP;
  components new Msp430Uart0C() as Resourceusart0;
  components new TimerMilliC() as TimerC0;
  components new TimerMilliC() as TimerC1;
  components HplMsp430GeneralIOC as GIO;
  co2sensorP.P20->GIO.Port20;
  co2sensor = co2sensorP.co2sensor;
  co2Control = co2sensorP.co2Control;
  components HplMsp430Usart0C;
  co2sensorP.Usart -> HplMsp430Usart0C;
  co2sensorP.Interrupts -> HplMsp430Usart0C;
  co2sensorP.Timer0 -> TimerC0;
  co2sensorP.Timer1 -> TimerC1;
  co2sensorP.uart0resource -> Resourceusart0;  //test resource
  //components MyLedsC as LedsC;
  //co2sensorP.Leds->LedsC;
  co2Info = co2sensorP.co2Info;
}

 
